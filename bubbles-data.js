var faker = require('faker');
var R = require('ramda');

var EVENT_COUNT = 50;
var PROJECT_COUNT = 8;

var eventNames = ['angular', 'react', 'knockout', 'PR', 'retro', 'standup', 'jira', 'pivotal', 'bitbucket',
    'stash', 'github', 'gitlab', 'lodash', 'ramda', 'd3'];

function _randomArray(data) {
    var num = faker.random.number({min: 1, max: 3});

    var randomIds = [];
    for (var i=0; i<num; i++) {
        randomIds.push(faker.random.arrayElement(data));
    }

    return R.uniq(randomIds);
}

function generateEvents(count, projects) {
    var tags = ['framework', 'process', 'tool', 'library', 'testing'];
    var events = [];

    for (var i = events.length; i < count; i++) {
        events.push({
            id: i,
            name: faker.random.arrayElement(eventNames),
            project: faker.random.arrayElement(projects).name,
            description: faker.company.bs(),
            date: faker.date.recent(30),
            impact: faker.random.arrayElement(
                [-3, -3, -3, -2, -2, -2, -1, -1, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3]
            ),
            submitter: faker.name.findName(),
            tags: _randomArray(tags)
        });
    }

    return events;
}

function generateProjects(count) {
    var projects = [];

    for (var i = projects.length; i < count; i++) {
        projects.push({
            id: i,
            name: faker.company.companyName()
        });
    }

    return projects;
}

module.exports = function() {
    return {
        events: generateEvents(EVENT_COUNT, generateProjects(PROJECT_COUNT))
    };
};
